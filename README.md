# COBOTTA Software Development Environment (SDE) 

This project aims to develop a shared platform for collabrative resesearch and development on the [Denso COBOTTA](https://www.denso-wave.com/en/robot/product/collabo/cobotta.html) and [Ritsumeikan Global Innovation Research Organization (R-GIRO)](http://en.ritsumei.ac.jp/research/r-giro/) by providing a robust, scalable, virtualized, and documented environment to all contributors.

**Content:**

[[_TOC_]]

## Contribution Guidelines

Carefully read the contribution guidelines before pushing new code or requesting a merge. Details can be found in the contribution guide:

> [https://gitlab.com/emlab/COBOTTA/blob/devel/CONTRIBUTING.md](https://gitlab.com/emlab/COBOTTA/blob/devel/CONTRIBUTING.md)

## Reporting Issues

Please report any issue or future work by using the GitLab issue tracker:

> [https://gitlab.com/emlab/COBOTTA/issues](https://gitlab.com/emlab/COBOTTA/issues)

## Getting Started

Follow this step-by-step guide to perform the initial setup of the COBOTTA project on a development machine running [Ubuntu](https://www.ubuntu.com/).

> **Note 1:** The whole development environment of the COBOTTA project is containerized/virtualized using [Docker](https://www.docker.com/). This ensures that all the contributors work in the exact same software environment.

> **Note 2:** The COBOTTA robot and simulator interfaces are primarily implemented with [ROS](http://www.ros.org/).

> **Note 3:** The COBOTTA project relies heavily on both hardware 3D graphic acceleration and Nvidia [CUDA](https://developer.nvidia.com/cuda-toolkit), thus a discrete Nvidia GPU is highly recommended although Intel CPU graphic acceleration is also supported. In addition, virtual machines are only partially supported: 3D accelerated tools such as [Rviz](https://github.com/ros-visualization/rviz) and [Gazebo](http://gazebosim.org/) will crash at runtime unless the same virtual machine drivers are installed in both the Docker container and the virtual machine.

> **Note 4:** Although this initial setup is meant to be performed only once, you can run it again would you want to reset the development environment as the project evolves. However, make sure to backup your latest changes beforehand.

### Step 0: Verify the Prerequisites

**Mandatory:**

*   A development machine running Ubuntu 20.04 LTS (Focal Fossa) based on the AMD64 architecture.
*   Access to administrator privileges (`sudo`) on the Ubuntu machine.
*   Access to developer privileges on the GitLab project at https://gitlab.com/emlab/COBOTTA.

**Recommended:**

*   A [Denso COBOTTA](https://www.denso-wave.com/en/robot/product/collabo/cobotta.html) robot for full operability. If not, the COBOTTA simulator provides support for basic operations.
*   A Nvidia GPU capable of running CUDA 10.1 (compute capability >= 3.0), or newer, to accelerate 3D graphics and deep learning computing.
*   A properly configured gitlab.com account linked with your personal SSH key to push contributions to the project repository: https://docs.gitlab.com/ee/ssh/.

### Step 1: Set up the Development Environment

Set up the environment of the development machine with the following instructions.

1.   Install [Git](https://git-scm.com/) if necessary:

     ```shell
     sudo apt-get update && sudo apt-get install -y git
     ```

2.   Clone the COBOTTA project repository in your home folder:

     ```shell
     cd ~/ && git clone https://gitlab.com/emlab/COBOTTA.git
     ```

     Enter your GitLab developer credentials if prompted.
3.   Configure the system environment:

     ```shell
     cd ~/COBOTTA/ && bash ./SETUP-DEVEL-MACHINE.bash
     ```

     The execution of `SETUP-DEVEL-MACHINE.bash` requires `sudo` permissions to install the tools that allow virtualization, i.e. Docker, [Docker Compose](https://github.com/docker/compose), and [Nvidia Docker 2](https://github.com/NVIDIA/nvidia-docker). System changes made with `sudo` are kept to a strict minimum.
4.   Reboot the system (or log out and back in) for the changes to users and groups to take effect:

     ```shell
     sudo reboot
     ```

> **Note 5:** The `SETUP-DEVEL-MACHINE.bash` script is actually divided into `INCL-SUDO-ENV.bash` and `INCL-USER-ENV.bash`. The execution of `INCL-SUDO-ENV.bash` makes system-wide changes and thus requires `sudo` permissions. However, if your system has already all the necessary tools installed, you can directly set up your local user environment with `cd ~/COBOTTA/ && bash ./INCL-USER-ENV.bash` which does not require `sudo` permissions.

> **Note 6:** You do not need to reboot if your local user has already been added to the `docker` group. If so, executing `docker --version` should not ask for `sudo`. In principle, you only need to reboot after the very first time you run `SETUP-DEVEL-MACHINE.bash`.

### Step 2: Build the Docker Image

Create a virtual environment using Docker (= Docker image) on the development machine with the following instructions.

1.   Build the Docker image:

     ```shell
     cd ~/COBOTTA/ && bash ./BUILD-DOCKER-IMAGE.bash
     ```

     This script builds the image following the instructions found in `~/COBOTTA/docker/cobotta-devel/Dockerfile`. Enter your GitLab developer credentials if prompted.

> **Note 7:** The script `BUILD-DOCKER-IMAGE.bash` first tries to download the image from the [container registry](https://gitlab.com/emlab/COBOTTA/container_registry) of the project. If, for whatever reason, the image cannot be downloaded, the script will build it locally. The later process is slow and can take up to 1 hour. In both cases, avoid using a Wi-Fi connection to greatly accelerate the process. Note that future local builds will reuse cached data whenever possible.

### Step 3: Run the Docker Container

Enter a virtual instance of the Docker image (= Docker container) on the development machine with the following instructions.

1.   Run the Docker container:

     ```shell
     cd ~/COBOTTA/ && bash ./RUN-DOCKER-CONTAINER.bash
     ```

     This script creates or updates the container following the instructions found in `~/COBOTTA/docker/docker-compose.yml`. It allows the container to share system resources, such as volumes and devices, with the host machine.
2.   Use `ctrl+d` to exit the container at any time.

> **Note 8:** If no Nvidia drivers are present, the script `RUN-DOCKER-CONTAINER.bash` sets the Docker runtime to `runc`, instead of `nvidia`, to bypass `nvidia-docker2` when entering the container. In this case, most 3D accelerated tools, including the COBOTTA simulator, will be extremely slow to run.

> **Note 9:** Be careful if you need to modify `docker-compose.yml` as the container will be recreated from scratch the next time you run `RUN-DOCKER-CONTAINER.bash`.

### Step 4: Test the Catkin Workspace

Build and test the ROS environment (= Catkin workspace) inside the Docker container with the following instructions.

1.   Enter the Docker container if necessary:

     ```shell
     cd ~/COBOTTA/ && bash ./RUN-DOCKER-CONTAINER.bash
     ```

2.   Initialize the Catkin workspace:

     ```shell
     cd /root/COBOTTA/ && bash ./docker/cobotta-devel/scripts/reset-catkin-workspace.bash
     ```

     This script will remove any existing Catkin workspace and build a new one inside `/root/COBOTTA/catkin_ws/`.
3.   Make sure that the new Catkin workspace is sourced:

     ```shell
     cd /root/COBOTTA/ && source ./catkin_ws/devel/setup.bash
     ```

3.   Launch a couple of publisher and subscriber test nodes from the `cobotta_launch` package:

     ```shell
     roslaunch cobotta_launch cobotta_chatter_default.launch
     ```

     If everything has installed correctly, you should see a node `cobotta_listener` that subscribes to a publisher node called `cobotta_talker` in a [Rqt](http://wiki.ros.org/rqt) GUI.

> **Note 10:** The script `reset-catkin-workspace.bash` will build the Catkin workspace using `catkin build` instead of the older but still official `catkin_make` compiler. Please be sure to build using `catkin build` to avoid strange issues.

### Step 5: Learn the Advanced Functions

The development environment inside the Docker container offers several useful functions that you should be aware of. These advanced functions will help you increase both the convenience and the quality of your work for the COBOTTA project.

#### Custom Bash Functions

The development environment contains the several useful Bash functions, all starting with the prefix `cobotta-`, to make your work more convenient. Including, but not limited to:

*   `cobotta-download-model-data`: Download from the cloud all the large binary files required at runtime (models, datasets, dictionaries, weights, etc.).
*   `cobotta-initialize-catkin-workspace`: Initialize, build, and source the Catkin workspace on top of the system ROS environment.
*   `cobotta-reset-catkin-workspace`: Remove built artifacts, then cleanly rebuild and source the Catkin workspace on top of the system ROS environment (to use after, for example, switching branches).
*   `cobotta-fix-permission-issues`: Fix the various permission issues that may appear when manipulating, on the host machine, files generated by the `root` user of the Docker container.
*   `cobotta-get-fully-started`: Execute several of the aforementioned functions to quickly get started when entering a freshly built Docker container.

> **Note 11:** These Bash functions are based on helper scripts that can be found in `/root/COBOTTA/docker/cobotta-devel/scripts/` in the Docker container or in `~/COBOTTA/docker/cobotta-devel/scripts/` in the host machine. You can see their definitions in `~/.bashrc` inside the container.

#### Multiple Terminal Operation

You can simultaneously run multiple terminals using `RUN-TERMINATOR-TERMINAL.bash`. This script opens [Terminator](https://gnometerminator.blogspot.com/) with the default layout configuration stored in `~/COBOTTA/terminator/config`. Each sub-terminal automatically executes `RUN-DOCKER-CONTAINER.bash` with a predefined ROS launch file for convenience. You can then select execution options by pressing specific keys as shown in the example below:

```
Run 'example_roslaunch_file.launch'? Press:
'r' to run with the robot,
's' to run in the simulator,
'c' to enter a child shell,
'q' to quit.
```

#### Configuration of the Docker Container

A cascade of scripts performs the initialization of the Docker container. Although the boundaries between them can sometimes be blurry, each one has a specific function and future implementations/revisions should keep these functions separated as much as possible. They can be found on the host machine at:

*   `~/COBOTTA/docker/cobotta-devel/Dockerfile`: Used by `BUILD-DOCKER-IMAGE.bash` to create the image of the shared development environment. It mainly describes the installations of the project tools/dependencies required inside the container.
*   `~/COBOTTA/docker/docker-compose.yml`: Describes the interface between the host machine and the container. It includes external information from outside the container, such as device host names or network configuration. It is invoked when the container is (re)started, most frequently the first time that `RUN-DOCKER-CONTAINER.bash` is run after the container is either stopped (host reboot) or rebuilt.
*   `~/COBOTTA/docker/cobotta-devel/scripts/initialize-docker-container.bash`: A startup script referenced in `docker-compose.yml` and thus also invoked when (re)starting the container, most frequently the first time that `RUN-DOCKER-CONTAINER.bash` is run after the container is either stopped (host reboot) or rebuilt. It executes the internal commands needed at the startup of the container (for example, to maintain the container running in the background).
*   `~/COBOTTA/docker/cobotta-devel/scripts/initialize-bash-shell.bash`: A shell script appended to `/root/.bashrc` at the end of the `Dockerfile` to set up the Bash shell environment inside the container (terminal colors, shell functions, symbolic links, binary paths, environment sourcing, etc.). Theoretically, its content could be written directly in the `Dockerfile` but this would result in complex lines of code as helpful Bash syntax like here-documents are not supported by Docker. It is automatically sourced by `/root/.bashrc` every time the user enters the Bash shell inside the container, most likely every time `RUN-DOCKER-CONTAINER.bash` is run.

> **Note 12:** The most important rule is to avoid pushing large binary files (datasets, weights, etc.) in the repository. Instead, you need to provide a link to download all your necessary large binary files from the cloud with `cobotta-download-model-data`. Ideally, all these files should be centralized in an online storage.

## How to Use the Robot 
* Use the Robot in ROS you need first to run the robot driver in its internal PC and set the ROS HOSTNAME to be able to control it from a remote PC.
### Commands to execute in the robot's internal PC
* Turn on the robot by the switch at the base of it.
* Connect by SSH to the robot's PC. The username and password are `emlab` and `em-admin+R`, respectively. The name of the PC are `cobotta-01.local` and `cobotta-02.local`
```
sss emlab@cobotta-01.local
```

* Initialize the robot driver.
```
sudo modprobe denso_cobotta io_power_mode=1
sudo chown emlab:emlab /dev/denso_cobotta
```
* Source the directories `source /opt/ros/melodic/setup.bash` and `source ~/catkin_ws/devel/setup.bash`
* Export the ROS Hostname  `export ROS_HOSTNAME=cobotta-01.local`
* Launch the robot bringup `roslaunch denso_cobotta_bringup denso_cobotta_bringup.launch`


> **Note 13:** The initialization of the robot driver only needs to be done once when the robot boot up.

> **Note 14:** To simplify the initialization of the robot, the script `BRINGUP-COBOTTA-ROBOT.bash` is provided. Running this script in a terminal will automatically connect to the COBOTTA robot, initialize the driver, source the files and bring up the robot.

> **Note 15:** To shutdown the robot, the script `SHUTDOWN-COBOTTA-ROBOT.bash` is provided. Running this script in a terminal will automatically connect to the COBOTTA robot and shut it down.
### Commands to execute in the external PC to control the robot from ROS

* Set `ROS_MASTER_URI` by executing `export ROS_MASTER_URI=http://cobotta-IP:11311`
* Set `ROS_IP` by executing `export ROS_IP=HOST_PC_IP`
* Check if you can see the topics and services published by the robot by doing `rostopic list` or `rosservice list`.

The previous commands are executed automatically when you enter the container using the following arguments.
`RUN-DOCKER-CONTAINER.bash $USER 1` or `RUN-DOCKER-CONTAINER.bash $USER 2`, to connect to COBOTTA 1 or COBOTTA 2, respectively.

> **Note 16:** To switch between simulation mode (to move the robot in simulation) or robot mode (to move the real robot). You just need to type the following alias in the terminal: `simulation_mode` or `robot_mode`.

*   Execute `cobotta_rqt_default.launch` or `cobotta_moveit_robot.launch`, and `cobotta_rviz_default.launch` using `roslaunch` to operate the robot from Rviz.

## How to Use the Simulator
*   Run the `cobotta_bringup_gazebo.launch` launch file using `roslaunch` to initialize the Gazebo simulation.
*   Execute `cobotta_rqt_default.launch` or `cobotta_moveit_robot.launch`, and `cobotta_rviz_default.launch` using `roslaunch` to operate the virtual robot from Rviz.

 > **Note 17:** Please check the official documentation at the [Denso COBOTTA website](https://densorobot.github.io/docs/denso_cobotta_ros/getting_started-melodic.html) in case you want to know more about the Installation of the robot, topics and services published and how to handle errors.

 ## How to tele-operate the robot using a 3D mouse.

 *   Execute the commands to control the robot from a external PC.
 *   Launch the cameras by executing `cobotta_cameras_default.launch`.
 *   Execute the command  `spacenavd`, this will start the communication between the 3D mouse and the PC (You can move the 3D mouse and nothing will happen).
 *   Execute `roslaunch rgiro_cobotta_utils rgiro_spacenave_cobotta.launch`. The robot will move as soon as it detect a movement in the [3D mouse.](https://3dconnexion.com/uk/spacemouse/)

 > **Note 18:** The robot may stop if you get too close to a singularity, collision with itself, or change abruptly of direction. This stop is the same as if you pressed the emergency button. In these cases the LED of the robot will turn red and the motors will turn off. In this state, you can move the robot manually to put it in a safe pose in case of being necessary.
 
 > **Note 19:** To activate again the robot. First, execute `rosservice call /cobotta/clear_error`. This will clear the error and the LED will turn green. Then, you will need to turn on the robot motors by executing `rosservice call /cobotta/set_motor_state "state: true"`. You will hear a click sound of the robot releasing the breaks and you can continue with the tele-operation.


 ## How to record and extract data from experiments
First we need to make sure that we can move the robot and all the sensors are working.
 * Execute the instructions of [How to tele-operate the robot using a 3D mouse.](#How-to-tele-operate-the-robot-using-a-3D-mouse.)
 * Launch the audio packages, first select the input and output on the settings of the host. Then, execute `roslaunch cobotta_launch cobotta_audio_default.launch`
 > **Note 20:** When running the command `roslaunch cobotta_launch cobotta_audio_default.launch`, the speaker will repeat the sound listened by the microphone.
 * Launch the force sensor package `roslaunch rgiro_force_sensor rgiro_force_sensor.launch`.

We will record the data in a rosbag file.  
 * Change directory to the package `rgiro_cobotta_utils/scripts`. 
 * Execute `bash record_rosbag.bash`, this script will record the images from the camera (RGB and depth), audio, joint angles, movements of the 3D mouse and tf. 

 > **Note 21:** By default the name of the rosbag will be cobotta_todays_date_time.bag. If you want to record a specific bag name you can write it as argument for the bash. 

To extract the data from the rosbag, we will use the scripts:
* `extract_single_rosbag_data.bash rosbag_file.bag`: To extract the information of only one rosbag file. This script will create a new folder with the name of the rosbag and there it will extract all the information.
* `extract_all_rosbags_data.bash directory_with_multiple_rosbag_files`: To extract the data from al the rosbags in a directory. This script finds all the .bag files in the directory and extracts the information recursively.
 > **Note 22:** For extracting the images from the rosbag, we save them temporarily in the .ros folder, then they are copy to the folder of the rosbag. So it is not possible to execute more than one script at the time, because the risk of overwriting the frame images.