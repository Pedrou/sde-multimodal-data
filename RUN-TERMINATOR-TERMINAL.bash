#!/bin/bash

# This script runs Terminator with a specific layout and sets 'COBOTTA_HOSTNAME' depending on [cobotta-number].
#
# Usage: bash RUN-TERMINATOR-TERMINAL.bash [cobotta-number]
#
# [cobotta-number]: Used to identify the COBOTTA robot to connect to.
#                   If '1', connect to 'cobotta-01.local' when entering the container with 'RUN-DOCKER-CONTAINER.bash'.
#                   If '2', connect to 'cobotta-02.local' when entering the container with 'RUN-DOCKER-CONTAINER.bash'.
#                   Otherwise, use the default host name.

################################################################################

################################################################################

# Make a backup of the user's original Terminator configuration.
mkdir -p ~/.config/terminator/
if [ ! -f ~/.config/terminator/config.backup ]; then
  cp ~/.config/terminator/config ~/.config/terminator/config.backup
fi

# Update the user's current Terminator configuration with the project's one.
cp ./terminator/config ~/.config/terminator/config

################################################################################

# Pass the cobotta-number as host name of the robot to the Terminator config file.
export COBOTTA_HOSTNAME=$1

# Run Terminator with the project's custom layout.
terminator -m -l cobotta-term-11 &
sleep 1
