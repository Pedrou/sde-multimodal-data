#!/bin/bash

################################################################################

# Tag the Docker image with the Git short SHA associated to the latest 'Dockerfile' modification.
DOCKER_FILE_HASH=`git log -n 1 --no-merges --pretty=format:%h ../docker/cobotta-devel/Dockerfile`
docker tag registry.gitlab.com/emlab/cobotta/cobotta-devel:latest registry.gitlab.com/emlab/cobotta/cobotta-devel:${DOCKER_FILE_HASH}

# Authenticate with the Docker image registry on GitLab using a deploy token.
docker login -u "gitlab+deploy-token-1868027" -p "9zAnGnyjgn8CUcVQg7TT" registry.gitlab.com/emlab/cobotta

# Push the latest Docker image in the registry.
docker push registry.gitlab.com/emlab/cobotta/cobotta-devel:${DOCKER_FILE_HASH}
docker push registry.gitlab.com/emlab/cobotta/cobotta-devel:latest
