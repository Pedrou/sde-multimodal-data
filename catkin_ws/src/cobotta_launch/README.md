# 'cobotta_launch' Package

The `cobotta_launch` package coordinates the robot tasks and behaviors across all packages using shared launch files.

*   Maintainer: Lotfi El Hafi ([lotfi.elhafi@gmail.com](mailto:lotfi.elhafi@gmail.com)).
*   Author: Lotfi El Hafi ([lotfi.elhafi@gmail.com](mailto:lotfi.elhafi@gmail.com)).
