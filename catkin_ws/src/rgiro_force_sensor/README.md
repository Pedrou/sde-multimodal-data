# rgiro_force_sensor

The `rgiro_force_sensor` contains the source code required to program the Arduino Nano Every connected to Noma-sensei's force sensor.
It also provides a roslaunch to connect to the sensor.

*   Maintainer: Pedro Uriguen ([puriguen@gmail.com](mailto:puriguen@gmail.com)).
*   Author: Pedro Uriguen ([puriguen@gmail.com](mailto:puriguen@gmail.com)).

## Content

[[_TOC_]]

## Purpose

The `rgiro_force_sensor` package goals are:

*    Provide the source code to use ROS with the force sensor.
*    Provide information related to the topics to subscribe and publish to use the sensor.

The source code was modified based on the Rosserial arduino package [documentation](http://wiki.ros.org/rosserial_arduino). Please refer to this documentation in case you need to reprogram the arduino.

> **Note 1:** In case you reprogram the arduino from linux, remember to change the permissions to the tty device from the host by executing `sudo chmod a+rw /dev/ttyACM*` in a terminal. This is a [video](https://youtu.be/sM53HkRRpUo) showing how to reprogram the Arduino using Linux (We assume that the Arduino IDE and the libraries of Rosserial Arduino and megaAVR boards are installed). 

> **Note 2:** In case you want to use the sensor only with the Arduino IDE and not ROS, you will need to reprogram it with the original source code from Noma-sensei. (The code is inside arduino_src directory)

## Rostopics published and subscribed by the node
The node publishes 2 topics:
*    `/force_sensor_data`: Publishes the data of the three channels from the sensor. The topic type is Int16MultyArray.
*    `/force_sensor_info`: Publishes string messages related to the status of the sensor, such as Errors or inputs received by the subscriber. 

The node subscribes to a topic related to how to control the sensor. Similar if we pressed the push buttons on the arduino board.
*    `/force_sensor_input`: Subscribes to the string messages sent to control the sensor, the possible messages are:
*    `1`: Keyboard input to correct the channel 1.
*    `2`: Keyboard input to correct the channel 2.
*    `3`: Keyboard input to correct the channel 3.
*    `a`: Keyboard input to correct all the channels.
*    `s`: Keyboard input not explained in the source code.


## Launch

*   `rgiro_force_sensor.launch`: Runs the nodes `rosserial_python` and communicates with the arduino.

> **Note 3:** The roslaunch is set to connect to an specific arduino by the serial number. To do a quick test with other arduino you can use the argument `dev:=/dev/ttyACM*`. If you are going to use the same sensor, it is recommended to modify the roslaunch with the serial number of the arduino.

