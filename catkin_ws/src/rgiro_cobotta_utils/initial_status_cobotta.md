Initial pose Cobotta

  position: 
    x: 0.312945798274
    y: -0.026137087124
    z: 0.0486286767052
  orientation: 
    x: -0.673341847292
    y: -0.667598495392
    z: -0.280389570207
    w: -0.149347562936

joints
[-0.5983590785722424, 0.6655259608561289, 1.9488049164850285, 2.72043941651678, 1.71376514941826, 0.4745497874019641]


Commands to move Cobotta
Close windows touchXmaster

Restart node in NUC


rostopic pub -1 /arm2_kinematics/set/desired_interpolator_speed std_msgs/Float64 "data: 1.0" 

rostopic pub -1 /arm2_kinematics/set/desired_pose geometry_msgs/PoseStamped "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
pose:
  position: 
    x: 0.312945798274
    y: -0.026137087124
    z: 0.0486286767052
  orientation: 
    x: -0.673341847292
    y: -0.667598495392
    z: -0.280389570207
    w: -0.149347562936
"

