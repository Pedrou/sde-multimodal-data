#!/usr/bin/env python3
# license removed for brevity
import rospy

import numpy as np
from audio_common_msgs.msg import AudioData, AudioInfo
import sys
# from IPython.terminal.debugger import set_trace as keyboard

def callback(msg_data):
    rospy.loginfo("I heard something")
    #Save data as an numpy uint8 array
    audio_values_uint8 = []
    audio_values_int16 = []
    audio_values_float32 = []
    for i in msg_data.data:
        audio_values_uint8.append(np.uint8(i))
        audio_values_int16.append(np.int16(i))
        audio_values_float32.append(np.float32(i))
    
    rospy.loginfo(audio_values_float32)



def listener():
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("audio/audio", AudioData, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
