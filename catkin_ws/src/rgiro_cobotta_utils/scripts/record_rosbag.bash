#! /bin/bash

#Save the rosbag with the date name in case no argument was received
dir_save="/root/COBOTTA/"
if (($# == 0)); 
then 
    echo "No name received using date as name"
    date=$(date '+%Y%m%d%H%M%S')
    name="cobotta"
else
    name=$1
fi


echo "Dir to save the rosbag"
echo ${name}

filename="${dir_save}${name}"
# echo ${filename}
topics="
    /rgiro_chatter
    /cobotta/joint_states
    /camera_side/color/image_raw/compressed
    /camera_ee_1/color/image_raw/compressed
    /camera_ee_2/color/image_raw/compressed
    /audio/audio
    /audio/audio_info
    /spacenav/joy
    /spacenav/twist
    /force_sensor_data
    /force_sensor_info
    /arm2/joints/get/joint_states
    /arm2_kinematics/get/pose
    /arm2_kinematics/set/desire_pose
    /ee_state
    /servo_server/delta_twist_cmds
    /rl_group/duration_all
    /usb_cam/camera_info
    /usb_cam/image_raw/compressed
    /ekew_i_driver/output 
    "

rosbag record -o ${name} ${topics}
# rosbag record -O ${filename} ${topics}




# /camera_top/depth/image_rect_raw/compressed/parameter_descriptions
# /camera_top/depth/image_rect_raw/compressed/parameter_updates
