#!/usr/bin/env python
import os
import sys
import numpy as np
import rospy
import actionlib
from std_msgs.msg import Bool
from sensor_msgs.msg import JointState
from denso_cobotta_gripper.msg import GripperMoveAction, GripperMoveGoal

class GripperController:

    def __init__(self):
        #Data from the gripper to fully open or fully close
        self.gripper_open = 0.015
        self.gripper_close = 0.0
        self.gripper_speed = 10.0
        self.gripper_effort = 10.0
        self.gripper_client = actionlib.SimpleActionClient('/cobotta/gripper_move', GripperMoveAction)
        self.gripper_state_now = 0.0
        self.plus_status = False
        self.minus_status = False

        #Get the state of the button
        rospy.Subscriber("/cobotta/minus_button", Bool, self.callback_minus, queue_size=1)
        rospy.Subscriber("/cobotta/plus_button", Bool, self.callback_plus, queue_size=1)
        #Get the current state of the gripper
        rospy.Subscriber("/cobotta/joint_states", JointState, self.callback_state)
        # self.control_gripper()

    def gripper_move(self, gripper_client, width, speed=10.0, effort=10.0):
        goal = GripperMoveGoal()
        goal.target_position = width
        goal.speed = speed
        goal.effort = effort
        gripper_client.send_goal(goal)

    def callback_state(self,data):
        if data.name[0] == "joint_gripper":
            #Rewrite the value of the gripper state
            self.gripper_state_now = np.around(data.position[0],decimals = 4)

    def callback_plus(self,data):
        self.plus_status = data.data
        if self.plus_status and not self.minus_status:
            #Plus button is pressed and minus button is not pressed
            rospy.loginfo("Opening")
            value_to_move = self.gripper_state_now + 0.0005
            if value_to_move <= self.gripper_open:
                self.gripper_move(self.gripper_client, value_to_move)

    def callback_minus(self,data):
        self.minus_status = data.data
        if self.minus_status and not self.plus_status:
            #Minus button is pressed and plus button is not pressed
            rospy.loginfo("Closing")
            value_to_move = self.gripper_state_now - 0.0005
            if value_to_move >= self.gripper_close:
                self.gripper_move(self.gripper_client, value_to_move)

if __name__ == '__main__':
    rospy.init_node("button_controller")
    gripper_controller = GripperController()
    rospy.spin()



    print("Bye...")
