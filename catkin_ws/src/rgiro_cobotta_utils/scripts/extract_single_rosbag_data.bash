#! /bin/bash
if (($# == 0)); 
then 
    echo "No name recieved, please try again"
    exit
fi
filename=$1



####Create folder with the name of the rosbag
echo $filename
folder_name=${filename:0:-4}
echo "Saving extracted files to folder:$folder_name"
mkdir -p ./$folder_name


#####################################################################
echo "Extract audio raw data"

rostopic echo -b $filename -p /audio/audio > ./$folder_name/audio_raw.cvs
rostopic echo -b $filename -p /audio/audio_info > ./$folder_name/audio_info.cvs

#####################################################################
echo "Extract joint information"

rostopic echo -b $filename -p /cobotta/joint_states > ./$folder_name/joint_states.cvs
rostopic echo -b $filename -p /spacenav/twist > ./$folder_name/spacenav_twist.cvs
#####################################################################
echo "Extract joint information Teleop "

rostopic echo -b $filename -p /arm2/joints/get/joint_states > ./$folder_name/joint_states_teleop.cvs
rostopic echo -b $filename -p /arm2_kinematics/get/pose > ./$folder_name/pose_teleop.cvs

#####################################################################
echo "Extract force sensor information"

rostopic echo -b $filename -p /force_sensor_data > ./$folder_name/force_sensor_data.cvs
rostopic echo -b $filename -p /force_sensor_info > ./$folder_name/force_sensor_log.cvs

#####################################################################
echo "Extract end-effector information"

rostopic echo -b $filename -p /ee_state > ./$folder_name/ee_state.cvs

#####################################################################
echo "Extract servo server information"

rostopic echo -b $filename -p /servo_server/delta_twist_cmds > ./$folder_name/servo_twist.cvs


#####################################################################
echo "Extract weight scale information"

rostopic echo -b $filename -p /ekew_i_driver/output  > ./$folder_name/weightscale.cvs

#####################################################################
echo "Extracting the video from the bag"

#Get duration of the rosbag
time_bag=$(rosbag info -y -k duration $filename)

#Get number of messages of this topic
message=$(rosbag info $filename |grep -E $video_topic| sed -r -e 's/  */ /g'| cut -d " " -f 3)




#Extract for camera ee_1
video_topic="/camera_ee_1/color/image_raw"
#Remove previous images in the .ros folder
rm -f ~/.ros/*.jpg
roslaunch rgiro_cobotta_utils export_video.launch bag_file_name:=$filename video_topic_name:=$video_topic video_transport_type:="compressed"

mkdir -p ./$folder_name/camera_ee_1_color
mv ~/.ros/*.jpg ./$folder_name/camera_ee_1_color/

#Extract for camera ee_2
video_topic="/camera_ee_2/color/image_raw"
#Remove previous images in the .ros folder

rm -f ~/.ros/*.jpg
roslaunch rgiro_cobotta_utils export_video.launch bag_file_name:=$filename video_topic_name:=$video_topic video_transport_type:="compressed"

mkdir -p ./$folder_name/camera_ee_2_color
mv ~/.ros/*.jpg ./$folder_name/camera_ee_2_color/

#Extract for camera top
video_topic="/camera_side/color/image_raw"
#Remove previous images in the .ros folder
rm -f ~/.ros/*.jpg
roslaunch rgiro_cobotta_utils export_video.launch bag_file_name:=$filename video_topic_name:=$video_topic video_transport_type:="compressed"

mkdir -p ./$folder_name/camera_side_color
mv ~/.ros/*.jpg ./$folder_name/camera_side_color/
# roslaunch 

video_topic="/usb_cam/image_raw"
#Remove previous images in the .ros folder
rm -f ~/.ros/*.jpg
roslaunch rgiro_cobotta_utils export_video.launch bag_file_name:=$filename video_topic_name:=$video_topic video_transport_type:="compressed"

mkdir -p ./$folder_name/camera_usb_top
mv ~/.ros/*.jpg ./$folder_name/camera_usb_top/
# roslaunch 
# /usb_cam/image_raw
#####################################################################

echo "Extracting the audio from the bag"
#Extract the audio
audio_output="${folder_name}/$folder_name.wav"
rosrun rgiro_cobotta_utils audio_extracter.py $filename $audio_output

chmod 777 -R ${folder_name}
