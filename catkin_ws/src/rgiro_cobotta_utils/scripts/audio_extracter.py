#!/usr/bin/env python2

from __future__ import division
from array import array
from pydoc_data.topics import topics

import sys
import numpy as np
import scikits.audiolab as al
import pyaudio
import rosbag
import warnings


if __name__ == '__main__':
    audio_wave_topic = "/audio/audio"
    audio_wave_topic_hz = 100
    load_format = np.float32
    audio_info_topic = "/audio/audio_info"

    bag_path = sys.argv[1]
    if len(sys.argv) < 3:
        out_path = sys.argv[1].replace('.bag', '.wav')
    else:
        out_path = sys.argv[2]


    in_bag = rosbag.Bag(bag_path)
    print('Output file: {0}'.format(out_path))

    sampling_rate = None

    # get audio information
    for topic, msg, t in in_bag.read_messages(topics=audio_info_topic):
        n_channel = msg.channels
        sampling_rate = msg.sample_rate
        sample_format = msg.sample_format
        bitrate = msg.bitrate
        coding_format = msg.coding_format
    # print(n_channel, sampling_rate, sample_format, bitrate, coding_format)

    if sampling_rate == None:
        print("Error: Not found Audio Info Topic")
        quit()

    wav = None
    t_s = in_bag.get_start_time()
    t_e = in_bag.get_end_time()
    n_frame = int(sampling_rate//audio_wave_topic_hz)

    # collect waveform
    for topic, msg, t in in_bag.read_messages(topics=audio_wave_topic):
        percentage = (t.to_sec() - t_s) / (t_e - t_s)
        sys.stdout.write('\r{0:4.2f}%'.format(100 * percentage))
        if wav is None:
            # Use Python lists instead of numpy arrays (for faster appending)
            wav = [list() for _ in range(n_channel)]
        # WARNING: please check dtype and sample_format are same!
        array = np.frombuffer(msg.data, dtype = load_format)
        new_data = array.reshape([len(array), n_channel])
        
        if len(new_data) > n_frame:
            warnings.warn("waveform is long")

        for i in range(n_channel):
            # Flatten list and append
            # https://stackoverflow.com/questions/952914/making-a-flat-list-out-of-list-of-lists-in-python
            for elem in np.transpose(new_data[:n_frame])[i]:
                wav[i].append(elem)
    sys.stdout.flush()

    # Columns: channels
    wav = np.transpose(np.array(wav))
    al.wavwrite(wav, out_path, fs=16000, enc="pcm16")
    print('\nDone!')
