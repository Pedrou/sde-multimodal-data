#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
from std_msgs.msg import String, Int16, Bool
from pynput.keyboard import Key, Listener

#Install the library pyinput using pip or pip2 install pynput in case of python2
#This code publishes a topic with true or false depending if a pedal is pressed or not
#you can only have one pedal pressed at the time


class KeyboardPublisher:

    def __init__(self):
        self.left_pedal_map = 'a'
        self.middle_pedal_map = 'b'
        self.right_pedal_map = 'c'
    
        # Initialise the node
        rospy.init_node("Pedal_publisher", anonymous=True)

        self.keyboard_publisher = rospy.Publisher('pedal', Bool, queue_size=1)

        # Create a publisher to the keypress topic
        self.l_pedal_publisher = rospy.Publisher('l_pedal', Int16, queue_size=1)
        self.r_pedal_publisher = rospy.Publisher('r_pedal', Int16, queue_size=1)
        self.m_pedal_publisher = rospy.Publisher('m_pedal', Int16, queue_size=1)


    def publish_l_pedal(self, key_press):
        self.l_pedal_publisher.publish(key_press)

    def publish_m_pedal(self, key_press):
        self.m_pedal_publisher.publish(key_press)

    def publish_r_pedal(self, key_press):
        self.r_pedal_publisher.publish(key_press)

    def publish_keypress(self, key_press):
        self.keyboard_publisher.publish(key_press)


    def on_press(self, key):
        #Obtain the pressed key
        try:
            key_detected = key.char  # single-char keys
        except:
            key_detected = key.name  # other keys
        
        print('{0} pressed'.format(key))

        if key_detected == self.left_pedal_map:
            self.publish_l_pedal(True)
        else:
            self.publish_l_pedal(False)

        if key_detected == self.right_pedal_map:
            self.publish_r_pedal(True)
        else:
            self.publish_r_pedal(False)
        
        if key_detected == self.middle_pedal_map:
            self.publish_m_pedal(True)
        else:
            self.publish_m_pedal(False)

    def on_release(self, key):
        print('{0} release'.format(
            key))
        if key == Key.esc:
            # Stop listener
            return False
        print('{0} release'.format(key))

        try:
            key_detected = key.char  # single-char keys
        except:
            key_detected = key.name  # other keys

        #If I release the pedal publish False
        if key_detected == self.left_pedal_map:
            self.publish_l_pedal(False)

        if key_detected == self.right_pedal_map:
            self.publish_r_pedal(False)
        
        if key_detected == self.middle_pedal_map:
            self.publish_m_pedal(False)


    def keyboard_listener(self):
        # Collect events until released
        with Listener(
            on_press=self.on_press,
            on_release=self.on_release) as listener:
            listener.join()


if __name__ == '__main__':
    keyboard_publisher = KeyboardPublisher()
    keyboard_publisher.keyboard_listener()
    try:
        rospy.spin()
    except rospy.ROSInterruptException:
        print("Stopping keyboard_publisher")




