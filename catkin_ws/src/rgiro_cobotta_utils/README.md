# rgiro_cobotta_utils

The `rgiro_cobotta_utils` package provides some useful scripts to quickly record, extract data and control the robot.

*   Maintainer: Pedro Uriguen ([puriguen@gmail.com](mailto:puriguen@gmail.com)).
*   Author: Pedro Uriguen ([puriguen@gmail.com](mailto:puriguen@gmail.com)).

## Content

[[_TOC_]]

## Purpose

The `rgiro_cobotta_utils` package goals are:

*    Provide useful scripts to record rosbags with multiple topics.
*    Extract data from the recorded rosbag to separated files.
*    Add functionalities to the Cobotta for its easy control

## Launch
*    `rgiro_spacenav_cobotta`: Allow us tele-operate the robot by running the moveit_servo node using a 3D mouse. The settings of the speed and sensibility of the robot are defined in the `/config/cobotta_navi_config.yaml` file.
*    `rgiro_gripper_controller.launch`: Runs the node `gripper_controller.py`, and open or closes the gripper depending the status of the +/- buttons. 
*    `rgiro_ps4_controller.launch`: Runs the moveit_joy node and communicates with a PS4 controller connected to the PC.

> **Note:** While running `rgiro_gripper_controller.launch` it is required to press and release the +/- button to open or close the gripper, a long press of the button may cause the gripper to not move. This is a issue to be fixed in the future.

## Bash scripts
*    These scripts need to be executed from inside the folder `rgiro_cobotta_utils/scripts`. 
*    `bash record_rosbag.bash`: This script will record the images from the camera (RGB and depth), audio, joint angles, movements of the 3D mouse and tf. 
> **Note:** By default the name of the rosbag will be cobotta_todays_date_time.bag. If you want to record a specific bag name you can write it as argument for the bash. 

*    `extract_single_rosbag_data.bash rosbag_file.bag #rosbag_name`: This script will extract the information of only one rosbag file. This script will create a new folder with the name of the rosbag and there it will extract all the information.
*    `extract_all_rosbags_data.bash directory_with_multiple_rosbag_files`: This script will extract the data from al the rosbags in a directory. This script finds all the .bag files in the directory and extracts the information recursively.

> **Note:** For extracting the images from the rosbag, we save them temporarily in the .ros folder, then they are copy to the folder of the rosbag. So it is not possible to execute more than one script at the time, because the risk of overwriting the frame images.