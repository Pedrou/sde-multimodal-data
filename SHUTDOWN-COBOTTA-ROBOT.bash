#!/bin/bash

# This script connects by SSH to the COBOTTTA 01 or 02 and runs the command to shut it down.
#
# Usage: bash SHUTDOWN-COBOTTA-ROBOT.bash [cobotta-number]
#
# [cobotta-number]: Used to identify the COBOTTA robot to connect to via SSH.
#                   If [cobotta-number] is '1', connect to 'cobotta-01.local'.
#                   If [cobotta-number] is '2', connect to 'cobotta-02.local'.
#                   Otherwise, an error message will appear saying that no arguments were supplied.

################################################################################

if [ $# -eq 0 ]
  then
    echo "No arguments supplied, please input the number of the COBOTTA to connect: 1 or 2."
    exit
  else
    echo "Argument detected, connecting with COBOTTA 0$1..."
    if [ $1 -eq 1 ]
      then
        COBOTTA_HOSTNAME=cobotta-01.local

        # Connect via SSH to the COBOTTA 01 robot to shutdown.
        ssh -t -l emlab ${COBOTTA_HOSTNAME} 'bash -c "
        sudo shutdown now;
        "'
    fi
    if [ $1 -eq 2 ]
      then
        COBOTTA_HOSTNAME=cobotta-02.local
        # Connect via SSH to the COBOTTA 02 robot to shutdown.
        ssh -t -l emlab ${COBOTTA_HOSTNAME} 'bash -c "
        sudo shutdown now;
        "'
    fi
    if [ $1 -eq 3 ]
      then
        COBOTTA_HOSTNAME=cobotta-03.local
        # Connect via SSH to the COBOTTA 02 robot to shutdown.
        ssh -t -l emlab ${COBOTTA_HOSTNAME} 'bash -c "
        sudo shutdown now;
        "'
    fi
fi
