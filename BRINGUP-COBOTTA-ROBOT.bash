#!/bin/bash

# This script connects by SSH to the COBOTTTA 01 or 02 and executes the commands to:
# 1. Initialize the robot driver.
# 2. Source the ROS environment.
# 3. Export the 'ROS_HOSTNAME'.
# 4. Execute the launch file that brings up the robot.
#
# Usage: bash BRINGUP-COBOTTA-ROBOT.bash [cobotta-number]
#
# [cobotta-number]: Used to identify the COBOTTA robot to connect to via SSH.
#                   If [cobotta-number] is '1', connect to 'cobotta-01.local'.
#                   If [cobotta-number] is '2', connect to 'cobotta-02.local'.
#                   Otherwise, an error message will appear saying that no arguments were supplied.

################################################################################
#The commands:
#        sudo modprobe denso_cobotta io_power_mode=1;
#        sudo chown emlab:emlab /dev/denso_cobotta;
#Were added directly to a service when the cobotta startup by using:
# echo -e '#!/bin/bash\nsudo modprobe denso_cobotta io_power_mode=1\nsudo chown emlab:emlab /dev/denso_cobotta' > ~/cobotta_startup.sh
# chmod +x ~/cobotta_startup.sh
# sudo bash -c "echo -e '@reboot root /home/emlab/cobotta_startup.sh\n' > /etc/cron.d/cobotta_startup"



if [ $# -eq 0 ]
  then
    echo "No arguments supplied, please input the number of the COBOTTA to connect: 1 or 2."
    exit
  else
    echo "Argument detected, connecting with COBOTTA 0$1..."
    if [ $1 -eq 1 ]
      then
        COBOTTA_NAME=cobotta-01.local

        # Connect via SSH to the COBOTTA 01 robot to run 'roscore'.
        ssh -t -l emlab ${COBOTTA_NAME} 'bash -c "
        source /opt/ros/melodic/setup.bash;
        source ~/catkin_ws/devel/setup.bash;
        export ROS_HOSTNAME=cobotta-01.local;
        killall -9 roscore;
        killall -9 rosmaster;
        roslaunch denso_cobotta_bringup denso_cobotta_bringup.launch;
        "'
    fi
    if  [ $1 -eq 2 ]
      then
        COBOTTA_NAME=cobotta-02.local

        # Connect via SSH to the COBOTTA 02 robot and run 'roscore'.
        ssh -t -l emlab ${COBOTTA_NAME} 'bash -c "
        source /opt/ros/melodic/setup.bash;
        source ~/catkin_ws/devel/setup.bash;
        export ROS_HOSTNAME=cobotta-02.local;
        killall -9 roscore;
        killall -9 rosmaster;
        roslaunch denso_cobotta_bringup denso_cobotta_bringup.launch;
        "'
    fi
    if  [ $1 -eq 3 ]
      then
        COBOTTA_NAME=cobotta-03.local

        # Connect via SSH to the COBOTTA 03 robot and run 'roscore'.
        ssh -t -l emlab ${COBOTTA_NAME} 'bash -c "
        source /opt/ros/melodic/setup.bash;
        source ~/catkin_ws/devel/setup.bash;
        export ROS_HOSTNAME=cobotta-03.local;
        killall -9 roscore;
        killall -9 rosmaster;
        roslaunch denso_cobotta_bringup denso_cobotta_bringup.launch;
        "'
    fi
fi
