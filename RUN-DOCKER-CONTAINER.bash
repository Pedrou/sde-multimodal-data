#!/bin/bash

# This script runs a Docker container instance with a name based on [docker-project].
#
# Usage: bash RUN-DOCKER-CONTAINER.bash [docker-project] [cobotta-number] [ros-launch]
#
# [docker-project]: Used to name the Docker container and create multiple instances if needed. Default value is '$USER'.
# [cobotta-number]: Used to determine to which COBOTTA to connect to. Can be '1' or '2'.
# [ros-launch]: Used to automatically preload a ROS launch file when entering the Docker container for convenience. Format is 'filename.launch'.

#################################################################################

# Set the default Docker runtime to use in './docker/docker-compose.yml'.
if [ -e /proc/driver/nvidia/version ]; then
  export DOCKER_RUNTIME=nvidia
else
  export DOCKER_RUNTIME=runc
fi

################################################################################

# Set the Docker container name from the [docker-project] argument.
# If no [docker-project] is given, use the current user name as the Docker project name.
DOCKER_PROJECT=$1
if [ -z "${DOCKER_PROJECT}" ]; then
  DOCKER_PROJECT=${USER}
fi
DOCKER_CONTAINER="${DOCKER_PROJECT}_cobotta_1"
echo "$0: DOCKER_PROJECT=${DOCKER_PROJECT}"
echo "$0: DOCKER_CONTAINER=${DOCKER_CONTAINER}"

# Run the Docker container in the background.
# Any changes made to './docker/docker-compose.yml' will recreate and overwrite the container.
docker-compose -p ${DOCKER_PROJECT} -f ./docker/docker-compose.yml up -d

################################################################################

# Configure the known host names with '/etc/hosts' in the Docker container.

# Attemp to connect to Harada-sensei's environment PC and add it to '/etc/hosts in the Docker container'
TELEOP_HOSTNAME=NUC-02.local              
echo "Now resolving local host name '${TELEOP_HOSTNAME}'..."
TELEOP_IP=`avahi-resolve -4 --name ${TELEOP_HOSTNAME} | cut -f 2`
if [ "$?" != "0" ]; then
  echo "Failed to execute 'avahi-resolve'. You may need to install 'avahi-utils'."
  docker exec -i ${DOCKER_CONTAINER} bash <<EOF
sed -i 's/TMP_TELEOP_HOSTNAME/${TELEOP_HOSTNAME}/' ~/.bashrc
EOF
elif [ ! -z "${TELEOP_IP}" ]; then
  echo "Successfully resolved host name '${TELEOP_HOSTNAME}' as '${TELEOP_IP}': '/etc/hosts' in the container is automatically updated."
  docker exec -i ${DOCKER_CONTAINER} bash <<EOF
sed -i 's/TMP_TELEOP_HOSTNAME/${TELEOP_HOSTNAME}/' ~/.bashrc
sed -n -e '/^[^#[:space:]]*[[:space:]]\+${TELEOP_HOSTNAME}\$/!p' /etc/hosts > /etc/hosts.tmp;
echo '${TELEOP_IP} ${TELEOP_HOSTNAME}' >> /etc/hosts.tmp
cp /etc/hosts.tmp /etc/hosts;
EOF
else
  echo "Failed to resolve host name '${TELEOP_HOSTNAME}': '/etc/hosts' in the container was not automatically updated."
fi


# Attemp to connect to COBOTTA 01 or COBOTTA 02.
COBOTTA_HOSTNAME=cobotta-0${2}.local
echo "Now resolving local host name '${COBOTTA_HOSTNAME}'..."
COBOTTA_IP=`avahi-resolve -4 --name ${COBOTTA_HOSTNAME} | cut -f 2`
if [ "$?" != "0" ]; then
  echo "Failed to execute 'avahi-resolve'. You may need to install 'avahi-utils'."
  docker exec -i ${DOCKER_CONTAINER} bash <<EOF
sed -i 's/TMP_COBOTTA_HOSTNAME/${COBOTTA_HOSTNAME}/' ~/.bashrc
EOF
elif [ ! -z "${COBOTTA_IP}" ]; then
  echo "Successfully resolved host name '${COBOTTA_HOSTNAME}' as '${COBOTTA_IP}': '/etc/hosts' in the container is automatically updated."
  docker exec -i ${DOCKER_CONTAINER} bash <<EOF
sed -i 's/TMP_COBOTTA_HOSTNAME/${COBOTTA_HOSTNAME}/' ~/.bashrc
sed -i 's/cobotta-01.local/${COBOTTA_HOSTNAME}/' ~/.bashrc
sed -i 's/cobotta-02.local/${COBOTTA_HOSTNAME}/' ~/.bashrc
sed -n -e '/^[^#[:space:]]*[[:space:]]\+${COBOTTA_HOSTNAME}\$/!p' /etc/hosts > /etc/hosts.tmp;
echo '${COBOTTA_IP} ${COBOTTA_HOSTNAME}' >> /etc/hosts.tmp
cp /etc/hosts.tmp /etc/hosts;
EOF
else
  echo "Failed to resolve host name '${COBOTTA_HOSTNAME}': '/etc/hosts' in the container was not automatically updated."
fi
                    
echo "The value of 'COBOTTA_HOSTNAME' is ${COBOTTA_HOSTNAME}."
echo "The value of 'TELEOP_HOSTNAME' is ${TELEOP_HOSTNAME}."
echo "------------------------------------------------------------"
################################################################################
# Display GUIs through X Server by granting full access to any external client.
xhost +

################################################################################

# Enter the Docker container with a Bash shell (with or without preloading a custom [ros-launch] file).
case "$3" in
  ( "" )
  docker exec -i -t ${DOCKER_CONTAINER} bash
  ;;
  ( "cobotta_chatter_default.launch" | \
    "cobotta_bringup_gazebo.launch" | \
    "cobotta_bringup_robot.launch" | \
    "cobotta_moveit_default.launch" | \
    "cobotta_rviz_default.launch" | \
    "cobotta_cameras_default.launch" | \
    "cobotta_rqt_default.launch" | \
    "cobotta_teleop_default.launch" | \
    "cobotta_force_sensor_default.launch" | \
    "cobotta_drill_default.launch" | \
    "cobotta_weightscale_default.launch" | \
    "cobotta_audio_default.launch")
  docker exec -i -t ${DOCKER_CONTAINER} bash -i -c "source ~/COBOTTA/docker/cobotta-devel/scripts/run-roslaunch-repeatedly.bash $3"
  ;;
  ( * )
  echo "Failed to enter the Docker container '${DOCKER_CONTAINER}': '$3' is not a valid argument value."
  ;;
esac
