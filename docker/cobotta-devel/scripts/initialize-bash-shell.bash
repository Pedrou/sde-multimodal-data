#!/bin/bash

################################################################################

# Link the default shell 'sh' to Bash.
alias sh='/bin/bash'

################################################################################

# Configure the terminal.

# Disable flow control. If enabled, inputting 'ctrl+s' locks the terminal until inputting 'ctrl+q'.
stty -ixon

################################################################################

# Configure 'umask' for giving read/write/execute permission to group members.
umask 0002

################################################################################

# Display the Docker container version.

# If switched from Dockerfile-sensitive generation to folder-sensitive generation,
# then show latest commit of the 'thr-devel' folder with 'git log -n 1 --format="%h %aN %s %ad" -- $directory'.
DOCKERFILE_LATEST_HASH=$(git -C /root/COBOTTA/ log -n 1 --no-merges --pretty=format:%h ./docker/cobotta-devel/Dockerfile)
DOCKERFILE_LATEST_DATE=$(git -C /root/COBOTTA/ log -n 1 --no-merges --pretty=format:%cd ./docker/cobotta-devel/Dockerfile)
DOCKERFILE_CREATION_DATE=$(git -C /root/COBOTTA/ show --no-patch --no-notes --pretty='%cd' ${DOCKER_IMAGE_VERSION})

echo -e "Container version: cobotta-devel:${DOCKER_IMAGE_VERSION:0:7} from ${DOCKERFILE_CREATION_DATE}"
if [[ "${DOCKERFILE_CREATION_DATE}" != "${DOCKERFILE_LATEST_DATE}" ]]; then
  echo -e "Newer image available: cobotta-devel:${DOCKERFILE_LATEST_HASH} from ${DOCKERFILE_LATEST_DATE}"
fi

################################################################################

# Source the ROS environment.
echo "Sourcing the ROS environment from '/opt/ros/noetic/setup.bash'."
source /opt/ros/noetic/setup.bash

# Source the Catkin workspace.
echo "Sourcing the Catkin workspace from '/root/COBOTTA/catkin_ws/devel/setup.bash'."
source /root/COBOTTA/catkin_ws/devel/setup.bash

################################################################################

# Add the Catkin workspace to the 'ROS_PACKAGE_PATH'.
export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:/root/COBOTTA/catkin_ws/src/

################################################################################

# Define Bash functions to conveniently execute the helper scripts in the current shell process.

function cobotta-fix-git-paths () {
  # Store the current directory and execute scripts in the current shell process.
  pushd .
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-git-paths.bash
  popd
}

function cobotta-initialize-catkin-workspace () {
  # Store the current directory and execute scripts in the current shell process.
  pushd .
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-git-paths.bash
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-permission-issues.bash
  source /root/COBOTTA/docker/cobotta-devel/scripts/initialize-catkin-workspace.bash
  popd
}

function cobotta-reset-catkin-workspace () {
  # Store the current directory and execute scripts in the current shell process.
  pushd .
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-git-paths.bash
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-permission-issues.bash
  source /root/COBOTTA/docker/cobotta-devel/scripts/reset-catkin-workspace.bash
  popd
}

function cobotta-fix-permission-issues () {
  # Store the current directory and execute scripts in the current shell process.
  pushd .
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-git-paths.bash
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-permission-issues.bash
  popd
}

function cobotta-download-model-data () {
  # Store the current directory and execute scripts in the current shell process.
  pushd .
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-permission-issues.bash
  source /root/COBOTTA/docker/cobotta-devel/scripts/download-model-data.bash
  popd
}

function cobotta-get-fully-started () {
  # Store the current directory and execute scripts in the current shell process.
  pushd .
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-git-paths.bash
  source /root/COBOTTA/docker/cobotta-devel/scripts/fix-permission-issues.bash
  source /root/COBOTTA/docker/cobotta-devel/scripts/download-model-data.bash
  source /root/COBOTTA/docker/cobotta-devel/scripts/reset-catkin-workspace.bash
  popd
}

################################################################################

# Set COBOTTA/ROS network interface.

# Look for the robot host name inside the local network and resolve it to get its IP address.
# The value of 'COBOTTA_HOSTNAME' should be initialized in '~/.bashrc' by './RUN-DOCKER-CONTAINER.bash' when entering the container.
COBOTTA_IP=`getent hosts ${COBOTTA_HOSTNAME} | cut -d ' ' -f 1`

if [ -z "${COBOTTA_IP}" ]; then
  # If no robot host name is found, set the local 'ROS_IP' using the default Docker network interface ('docker0').
  export ROS_IP=$(LANG=C /sbin/ifconfig docker0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
else
  # If the robot host name is found, set the local 'ROS_IP' using the network interface that connects to the robot.
  # TODO: Use Bash instead of Python.
  export ROS_IP=`python3 /root/COBOTTA/docker/cobotta-devel/scripts/print-interface-ip.py ${COBOTTA_IP}`
fi
if [ -z "${ROS_IP}" ]; then
  # If the local 'ROS_IP' is still empty, default to the Docker network interface ('docker0') for sanity.
  export ROS_IP=$(LANG=C /sbin/ifconfig docker0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
fi
echo "ROS_IP is set to '${ROS_IP}'."

# echo "The value of 'COBOTTA IP' is ${COBOTTA_IP}."
# echo "The value of 'COBOTTA_HOSTNAME' is ${COBOTTA_HOSTNAME}."
# echo "The value of 'TELEOP_HOSTNAME' is ${TELEOP_HOSTNAME}."

export ROS_HOME=~/.ros

alias simulation_mode='export ROS_MASTER_URI=http://localhost:11311; export PS1="\[[44;1;37m\]<local>\[[0m\]\w$ "'
alias robot_mode='export ROS_MASTER_URI=http://${COBOTTA_HOSTNAME}:11311; export PS1="\[[41;1;37m\]<cobotta>\[[0m\]\w$ "'
alias teleop_mode='export ROS_MASTER_URI=http://${TELEOP_HOSTNAME}:11311; export PS1="\[[42;1;37m\]<teleop>\[[0m\]\w$ "'
################################################################################

# Move to the working directory.
cd /root/COBOTTA/
