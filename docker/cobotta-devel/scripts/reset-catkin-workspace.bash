#!/bin/bash

################################################################################

# Download package lists from Ubuntu repositories.
apt-get update

# Install system dependencies required by specific ROS packages.
# http://wiki.ros.org/rosdep
rosdep update

# Source the updated ROS environment.
source /opt/ros/noetic/setup.bash

################################################################################

# Remove the Catkin workspace.

# Delete unexpected 'catkin_make' artefacts.
cd /root/COBOTTA/catkin_ws/ && rm -r .catkin_workspace build/ devel/ install/
cd /root/COBOTTA/catkin_ws/src/ && rm CMakeLists.txt

# Delete expected 'catkin build' artefacts.
cd /root/COBOTTA/catkin_ws/ && catkin clean -y
cd /root/COBOTTA/catkin_ws/ && rm -r CMakeLists.txt .catkin_tools/

################################################################################

# Initialize and build the Catkin workspace.
cd /root/COBOTTA/catkin_ws/ && catkin_init_workspace 

# Build the Catkin workspace.
source /root/COBOTTA/docker/cobotta-devel/scripts/build-catkin-workspace.bash
